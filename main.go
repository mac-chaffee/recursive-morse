package main

import (
	"fmt"
	"io/ioutil"
	"runtime"
	"strings"
)

var code = map[string]string{
	" ":      " ",
	".----.": "'",
	".-.-.-": ".",
	"..--..": "?",
	"_._.--": "!",
	".-":     "A",
	"-...":   "B",
	"-.-.":   "C",
	"-..":    "D",
	".":      "E",
	"..-.":   "F",
	"--.":    "G",
	"....":   "H",
	"..":     "I",
	".---":   "J",
	"-.-":    "K",
	".-..":   "L",
	"--":     "M",
	"-.":     "N",
	"---":    "O",
	".--.":   "P",
	"--.-":   "Q",
	".-.":    "R",
	"...":    "S",
	"-":      "T",
	"..-":    "U",
	"...-":   "V",
	".--":    "W",
	"-..-":   "X",
	"-.--":   "Y",
	"--..":   "Z",
	"-----":  "0",
	".----":  "1",
	"..---":  "2",
	"...--":  "3",
	"....--": "4",
	".....":  "5",
	"-....":  "6",
	"--...":  "7",
	"---..":  "8",
	"----.":  "9",
}

var morseConverter = map[string]string{
	"DOT":   ".",
	"DASH":  "-",
	"SPACE": " ",
}

// Read the file and start decoding
func main() {
	rawText, err := ioutil.ReadFile("code.txt")
	if err != nil {
		panic(err)
	}
	text := string(rawText)
	letters := splitMorse(text)
	fmt.Println(decode(letters))
}

// Chop up the text into NumCPU chunks
// Won't work yet because when n > 1, words get cut off
func createChunks(letters []string) [][]string {
	chunks := make([][]string, 0)
	numLetters := len(letters)
	cpus := runtime.NumCPU()
	chunkSize := numLetters/cpus + 1

	for i := 0; i < numLetters; {
		end := i + chunkSize
		if end > numLetters {
			end = numLetters
		}
		// Must make the cut between words so no words get split between chunks
		for letters[end-1] != " " && end <= numLetters {
			end++
		}
		chunks = append(chunks, letters[i:end])
		i = end
	}
	return chunks
}

// Convert the text into a list of morse letters
func splitMorse(text string) []string {
	words := strings.Split(text, "       ")
	letters := make([]string, 0)
	for _, word := range words {
		letters = append(letters, strings.Split(word, " ")...)
		letters = append(letters, " ")
	}
	// Remove the trailing delimiter
	return letters[:len(letters)]
}

// Recursively convert morse code to DOTs and DASHes, then back
// to morse, then back to DOTs and DASHes until the real message appears
func decode(letters []string) string {
	var builder strings.Builder
	for _, l := range letters {
		builder.WriteString(code[l])
	}
	decodedText := builder.String()
	if strings.HasPrefix(decodedText, "DOT") ||
		strings.HasPrefix(decodedText, "DASH") ||
		strings.HasPrefix(decodedText, "SPACE") {
		return decode(convertToMorse(decodedText))
	}
	return decodedText
}

// Convert the words DOT and DASH into their respective symbols
func convertToMorse(text string) []string {
	var builder strings.Builder
	for _, word := range strings.Split(text, " ") {
		builder.WriteString(morseConverter[word])
	}
	return splitMorse(builder.String())
}
